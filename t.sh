#!/bin/sh
#
# test with valgrind
#
# to use need to:
#    1. enable -g -O0
#    2. disable optimizations in compile
#

. $HOME/src/util/tsetup.sh

g_test_memory="Y"

#
# main
#

g_log_valgrind=$TMPDIR/valgrind.txt
g_test_kdump="N"
g_ktrace=""

cd $HDIR
f_ckprog ./add_day

if test "$HAVE_VALGRIND" = "N"
then
    g_test_memory="N"
    if test "$OS" = "OpenBSD"
    then
	if test ! -f Makefile
	then
	    f_msg "E100: START ./add_day, Makefile missing for kdump test"
	fi
	grep '^CFLAGS=' < Makefile | grep ' -g ' > /dev/null 2>&1
	if test "$?" -eq "0"
	then
	    g_ktrace="ktrace -tu"
	    g_test_kdump="Y"
	    MALLOC_OPTIONS="D"
	    export MALLOC_OPTIONS
	else
	    g_test_kdump="N"
	    unset MALLOC_OPTIONS
	fi
    fi
else
    grep '^CFLAGS=' < Makefile | grep ' -g ' > /dev/null 2>&1
    if test "$?" -ne "0"
    then
	g_test_memory="N"
    fi
fi

if test "$g_test_memory" = "Y"
then
    f_msg "I200: START valgrind ./add_day"
    f_msg "A201: START valgrind ./add_day" > "$g_log_valgrind"
    valgrind -s --tool=memcheck --leak-check=yes        \
                --leak-check=full --show-leak-kinds=all \
                --track-origins=yes                     \
                --keep-stacktraces=alloc-then-free      \
		./add_day -L 20220227 -M 20220310 \
                >> "$g_log_valgrind" 2>&1
    f_msg "B290: END   valgrind ./add_day"     >> "$g_log_valgrind"
    f_msg "I291: END   valgrind ./add_day"
    if test "$TERM" = "dumb"
    then
	cat $g_log_valgrind
    fi
    f_msg "I291: Created: $g_log_valgrind"
else
    f_msg "I300: START ./add_day, testing $OS memory = $g_test_kdump"
    $g_ktrace ./add_day -L 20240227 -M 20240305
    if test "$g_test_kdump" = "Y"
    then
	sleep 1
	kdump -u malloc
	sleep 1
	f_msg "I313: ***** Execute this:"
	f_msg "I314:       addr2line -e ./add_day 0x????"
	f_msg "I315: ***** using the last hex val on the line for ./add_day"
	f_msg "I316: ***** to see where the leak is (if any)"
    fi
    f_msg "I320: END   ./add_day, testing $OS memory = $g_test_kdump"
fi

exit 0
