## add\_day - List sequential dates for test input

Repositories:
* [gitlab repository](https://gitlab.com/jmcunx1/add_day) (this site)
* gemini://gem.sdf.org/jmccue/computer/repoinfo/add\_day.gmi (mirror)
* gemini://sdf.org/jmccue/computer/repoinfo/add\_day.gmi (mirror)

add\_day(1) is a silly utility that prints all dates between
a low/high range.

This can be useful in scripts that need a list of Dates
for testing or other purposes.

[j\_lib2](https://gitlab.com/jmcunx1/j_lib2)
is an **optional** dependency.

[Automake](https://en.wikipedia.org/wiki/Automake)
only confuses me, but this seems to be good enough for me.

To compile:
* If "DESTDIR" is not set, will install under /usr/local
* Execute ./build.sh to create a Makefile,
execute './build.sh --help' for options
* Then make and make install
* Works on Linux, BSD and AIX

To uninstall:
* make (see compile above)
* As root: 'make uninstall' from the source directory

This is licensed using the
[ISC Licence](https://en.wikipedia.org/wiki/ISC_license).
